// rdo_test.go
package rdo

import (
	"testing"
)

func TestCalcRdoWd(t *testing.T) {
	//yl=wd
	dat := map[int]int{-12: 0, 0: 0, 1081: 280, 1111: 287, 54886: 13730, 54896: 13730}
	for ky, vw := range dat {
		if wd := CalcRdoWd(ky); wd != vw {
			t.Fail()
			t.Log(ky, wd, vw)
		}
	}
}
